import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})
export class FirstComponent implements OnInit {

  @Input() appareil : string;
  @Output() ratingAppareil : EventEmitter<string> = new EventEmitter<string>(); // Event pour soumettre les modifications
  age : number = 12;
  appareils : Array<string> = ["Machine à laver", "TV", "Ordinateur", "Aspirateur", "Voiture"]

  constructor() { 
  }

  onRatingAppareil(){
    console.log("FirstComponent ==>", this.appareil );
    this.ratingAppareil.emit(this.appareil);
  }

  showAppareilValue(): void {
    alert(this.appareil);
  }

  ngOnInit(): void {
  }



}
