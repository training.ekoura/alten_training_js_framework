import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../../models/user';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  
  @Input() nUser : User = new User();
  @Output() ratingNewUser : EventEmitter<User> = new EventEmitter<User>();

  constructor() { 
   
  }

  ngOnInit(): void {
  }

  onSave(){
    this.ratingNewUser.emit(this.nUser);
    this.nUser = new User();
  }

}
