import { Component, OnInit } from '@angular/core';
import { User } from 'src/models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  myUser : User = new User();
  myUsers : User[] = [];

  

  constructor() { }

  ngOnInit(): void {
  }

  catchAndAddNewUser(nValue : User){
    this.myUsers.push(nValue);
  }

  

}
