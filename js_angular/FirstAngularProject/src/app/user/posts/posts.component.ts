import { Component, OnInit } from '@angular/core';
import { IPost } from 'src/models/user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts : IPost[] = [];

  constructor(private _userService : UserService) { }

  ngOnInit(): void {
    this._userService.getPost().subscribe((response) => {
      this.posts = response as IPost[];
    });
  }

}
