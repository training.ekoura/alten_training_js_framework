import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUsersURl : string = "https://jsonplaceholder.typicode.com/users";
  apiPostsURl : string = "https://jsonplaceholder.typicode.com/posts";
  constructor(private _httpClient : HttpClient) { 

  }

  getUsers() {
    return this._httpClient.get(this.apiUsersURl);
  }

  getPost() {
    return this._httpClient.get(this.apiPostsURl);
  }
}
