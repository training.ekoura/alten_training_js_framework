import { Component, OnInit} from '@angular/core';
import { IProduct } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products : IProduct[] = [];
  tempProducts : IProduct[] = [];

  /////////////////////////////////////////////////
  // For filter
  private _searchProduct: string;
  public get searchProduct(): string {
    return this._searchProduct;
  }
  public set searchProduct(value: string) {
    this.tempProducts = (value.length == 0) ? this.products : this.products.filter(product => product.productName.toLowerCase().includes(value.toLowerCase()))
    this._searchProduct = value;
  }
  /////////////////////////////////////////////////
  showImage = false;

  constructor(private _productService : ProductService) { }

  ngOnInit(): void {
    // Load Product service
    this._productService.getProduct().subscribe((response) => {
      this.products = response;
      this.tempProducts = this.products;
    })
  }

}
