import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IProduct } from './product';
import { map, tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  productApiUrl = "../../assets/api/products/products.json";
  constructor(private _httpClient :HttpClient) { }

  getProduct() : Observable<IProduct[]> {
    return this._httpClient.get<IProduct[]>(this.productApiUrl);
  }

  getProductById(id : number){
    return this.getProduct()
                .pipe(
                  map(products=> products.find(prod => prod.productId == id))
                );
  }
}
