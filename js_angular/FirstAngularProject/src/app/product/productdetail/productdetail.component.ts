import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user/user.service';
import { ProductService } from '../product.service';
import { IProduct } from '../product';

@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.scss']
})
export class ProductdetailComponent implements OnInit {

  product : IProduct;

  constructor(private _route : ActivatedRoute, private _router : Router, private _productService : ProductService) { 

    // Optional params
    //console.log("id ==> ", this._route.snapshot.queryParams)
    //console.log("id ==> " + this._route.snapshot.paramMap.get("id"))
    
    this._route.paramMap.subscribe(data => {

      let id = Number.parseInt(data.get("id"));
      this._productService.getProductById(id).subscribe(response => {
        this.product = response;
      });
    
    })

  }

  ngOnInit(): void {
  }

  backButton(){
    let id = Number.parseInt(this._route.snapshot.paramMap.get("id"));
    this._router.navigate(["/catalogue"]);
    //this._router.navigate(["/catalogue", id]);

    // Optional params
    //this._router.navigate(["/catalogue", {selectedProduct : this._route.snapshot.paramMap.get("id")}]);
    
    // Javascript back 
    //history.go(-1);
  }

  next() {

  }

  prev(){

  }

}
