import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateUserComponent } from './user/create-user/create-user.component';
import { ListUserComponent } from './user/list-user/list-user.component';
import { ConvertToSpacePipe } from './shared/convert-to-space.pipe';
import { HttpClientModule } from '@angular/common/http';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductdetailComponent } from './product/productdetail/productdetail.component';
import { HomeComponent } from './site/home/home.component';
import { PageNotFoundComponent } from './site/page-not-found/page-not-found.component';
import { NavigationComponent } from './site/navigation/navigation.component';
import { UsersComponent } from './user/users/users.component';
import { PostsComponent } from './user/posts/posts.component';
import { LoginComponent } from './site/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    CreateUserComponent,
    ListUserComponent,
    ConvertToSpacePipe,
    ProductListComponent,
    ProductdetailComponent,
    HomeComponent,
    PageNotFoundComponent,
    NavigationComponent,
    UsersComponent,
    PostsComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
