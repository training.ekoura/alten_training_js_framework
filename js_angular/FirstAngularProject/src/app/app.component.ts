import { Component } from '@angular/core';
 
@Component({
  selector: 'app-root',
  //template: `<h1> Nous sommes en formation Angular !!!! :)</h1>`,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  constructor() {}

  ngOnInit(){}

  /*
    ngAfterContentInit(){
      console.log("Enter ngAfterContentInit");
    }
    ngAfterViewInit(){
      console.log("Enter ngAfterViewInit");
    }
    ngOnDestroy(){
      console.log("Enter ngOnDestroy");
    }
  */
}
