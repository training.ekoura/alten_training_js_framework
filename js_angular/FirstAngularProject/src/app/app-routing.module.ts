import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductdetailComponent } from './product/productdetail/productdetail.component';
import { HomeComponent } from './site/home/home.component';
import { PageNotFoundComponent } from './site/page-not-found/page-not-found.component';
import { UsersComponent } from './user/users/users.component';
import { PostsComponent } from './user/posts/posts.component';
import { ProductGuard } from './product/product.guard';
import { LoginComponent } from './site/login/login.component';

const routes: Routes = [
  { path : 'catalogue', component : ProductListComponent , canActivate: [ProductGuard]},
  { path : 'login', component : LoginComponent },
  //{ path : 'product/:id/:code', component : ProductdetailComponent},
  { path : 'product/:id', component : ProductdetailComponent},
  { path : 'welcome', component : HomeComponent},
  { path : 'users', component : UsersComponent},
  { path : 'posts', component : PostsComponent},
  { path : '', redirectTo : 'welcome', pathMatch : 'full'},
  { path : '**', component : PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
