import { ValidatorFn } from '@angular/forms';
export const validUserTitle: ValidatorFn = (control) => {
    
    /* Is not valid. */
    if (/learn .*? in one day/i.test(control.value)) {
        return {
            'validUserTitle' : {
                reason : 'blacklisted',
                value: control.value
            }
        }
    }

    /* Is valid */
    return null;
}