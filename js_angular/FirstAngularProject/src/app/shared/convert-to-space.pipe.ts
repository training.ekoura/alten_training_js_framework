import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertToSpace'
})
export class ConvertToSpacePipe implements PipeTransform {

  //transform(value: string, ...character : string[]): string {
  transform(value: string, character : string): string {
    console.log(character);
    
    
    // 1ère méthode 
    /*
    let newValue = "";
    for (let i = 0; i < value.length; i++) {
      if(value.charAt(i) !== character){
        newValue += value.charAt(i);
        console.log(newValue);
        
      }

    }*/
    
    // 2ème méthode 
    //return value.split(character).join("");

    // 3ème méthode 
    return value.replace(new RegExp(character, 'g'), '');

    // 4ème boucle while.
  }

}
