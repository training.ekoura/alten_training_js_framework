import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validUserTitle } from 'src/app/shared/valid-user-title.validator';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Mode 
  mode = "login";
  // Reactive forms
  userForm = new FormGroup({
    title: new FormControl(null,[Validators.required, Validators.minLength(3)]),
    password: new FormControl(null, [Validators.required, validUserTitle]),
    nom: new FormControl(null,[Validators.required, Validators.minLength(3)]),
    prenom: new FormControl(null,[Validators.required, Validators.minLength(3)])
  });

  constructor(private _route : ActivatedRoute) { 

    this._route.queryParamMap.subscribe(data =>{
      this.mode = (data.get("mode")) ? data.get("mode") : 'login';
    })

  }

  ngOnInit(): void {
  }

  submitUser() {
    console.log(this.userForm.value);
  }

}
