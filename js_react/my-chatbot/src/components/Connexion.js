import React, { Component } from 'react';




class Connexion extends Component {

    componentDidUpdate(){
        if(this.state.goToChat)
            this.props.history.push("/pseudo/"+ this.state.pseudo)
    }

    state = {
        pseudo : '',
        goToChat : false
    }

    handleOnChange = event => {
        this.setState({pseudo : event.target.value})
    }

    handleOnSubmit = event => {
        event.preventDefault() // Cancel page reload
        this.setState({goToChat : true})
    }

    render() {
        return (
            <div className="connexionBox">
                <form className="connexion" onSubmit={this.handleOnSubmit}>
                    <input 
                        type="text"
                        value={this.state.pseudo}
                        onChange={this.handleOnChange}
                        placeholder="Pseudo"
                        required />
                    <button type="submit">GO</button>
                </form>
            </div>
        )
    }
}

export default Connexion

