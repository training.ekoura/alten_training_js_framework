import React, { Fragment } from 'react'

const Message = ({ message, pseudo }) => {
    
    return (
        <p className={pseudo === message.pseudo ? 'user-message' : 'not-user-message'}>
            { pseudo === message.pseudo 
                ? message.message 
                :   <Fragment>
                        <strong>{message.pseudo + ":" }</strong>
                        {message.message}
                    </Fragment>} 
        </p>
    )
}

export default Message