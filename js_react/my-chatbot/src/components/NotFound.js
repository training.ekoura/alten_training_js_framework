import React from 'react';

const NotFound = ()=> (
    <div style={{ paddingTop:'30px', display : 'flex', justifyContent : 'center' }}>
        <img src="https://cdn.pixabay.com/photo/2019/07/15/23/51/magnifying-4340698_960_720.jpg" />
    </div>
)

export default NotFound