import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Formulaire extends Component {
    state = {
        message : '',
        length : 140
    }

    createMessage = () => {
        const {addMessage} = this.props

        const message = {
            pseudo : this.props.match.params.pseudo,
            message : this.state.message
        }

        addMessage(message)

        this.setState({message : '', length : this.props.length})

        
    }
    
    handleOnSubmit = event => {
        event.preventDefault() // Empèche le reload de la page
        this.createMessage()
    }

    handleOnKeyUp = event => {
        if(event.key === 'Enter')
            this.createMessage()
    }

    handleOnChange = event => {
        this.setState({
                        message : event.target.value, 
                        length : this.props.length-event.target.value.length
                    })
    }

    render() {
        console.log(this.props, "<==== props from formulaire");
        return (
            <form className="form" onSubmit={this.handleOnSubmit}>
                <textarea 
                    value={this.state.message}
                    onChange={this.handleOnChange} 
                    onKeyUp={this.handleOnKeyUp}
                    required 
                    maxLength='140' />
                <div className="info">
                    {this.state.length}
                </div>
                <button type="submit">Envoyer !</button>
            </form>
        )
    }
}

export default withRouter(Formulaire)