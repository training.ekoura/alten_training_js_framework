import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import Connexion from './components/Connexion';
import NotFound from './components/NotFound';

ReactDOM.render(
  <Router>
    <div className="container-fluid">
      {/*<nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/"  >Navbar</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
            </li>
          </ul>
        </div>
      </nav>*/}
      <Switch>
        <Route exact path="/" component={Connexion} />
        <Route path="/pseudo/:pseudo" component={App} />
        <Route component={NotFound}></Route>
      </Switch>
    </div>
  </Router>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


/*



*/
