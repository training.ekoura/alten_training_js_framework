import React, { Component, createRef } from 'react';
import './App.css';
import Formulaire from './components/Formulaire';
import Message from './components/Message';


class App extends Component {
  
  messagesRef = createRef()

  componentDidMount(){
    if(localStorage.getItem("messages")){
      this.setState({
        messages : JSON.parse(localStorage.getItem("messages"))
      })
    }
  }

  componentDidUpdate() {
    const ref = this.messagesRef.current
    ref.scrollTop = ref.scrollHeight
  }

  componentWillUnmount() {
    console.log("app componentDidUpdate");
  }

  state = {
    messages : {}
  }

  addMessage = message => {
    const {messages} = this.state
    messages[`message-${Date.now()}`] = message
    this.setState({messages})

    localStorage.setItem("messages", JSON.stringify(messages));
  }

  render () {
    const {messages} = this.state

    const listeMessage  = Object
                          .keys(messages)
                          .map((keyValue, index)=>(
                              <Message 
                                key={index}
                                message={messages[keyValue]} 
                                pseudo={this.props.match.params.pseudo}/>
                          ))
    return (
      <div className='box' >
        <div>
          <div className="messages" ref={this.messagesRef}>
            <div className="message">
              {listeMessage}
            </div>
          </div>
        </div>
        <Formulaire length={140} 
                    pseudo={this.state.pseudo} 
                    addMessage={this.addMessage} />
      </div>
    )
  }
}

export default App;
