import * as ActionType from './action'

const initialState = {
    counter : 0,
    results : []
}

const rootReducer = (currentState = initialState, action)=> {

    switch (action.type) {
        case ActionType.INC_COUNTER:
            return {
                ...currentState,
                counter : ++currentState.counter
            }
        
        case ActionType.DEC_COUNTER:
            return {
                ...currentState,
                counter : --currentState.counter
            }
        
        case ActionType.ADD_COUNTER:
            return {
                ...currentState,
                counter : currentState.counter + action.val
            }
        
        case  ActionType.SUB_COUNTER: 
            return {
                ...currentState,
                counter : currentState.counter - action.val
            }


        case  ActionType.SAVE_COUNTER: 
            return {
                ...currentState,
                results : [...currentState.results, { id : Date.now(), value : currentState.counter}]
            }
    
        case  ActionType.DELETESAVED_COUNTER: 
            return {
                ...currentState,
                results : [...currentState.results].filter(result => result.id !== action.selectedLine)
            }
    }

    return currentState
}

export default rootReducer;