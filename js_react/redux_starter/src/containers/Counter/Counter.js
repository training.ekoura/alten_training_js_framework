import React, { Component } from 'react';
import * as ActionType from '../../store/action'


import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';
import { connect } from 'react-redux';

class Counter extends Component {
    
    render () {
        console.log(this.props)
        return (
            <div>
                <CounterOutput value={this.props.ctr} />
                <CounterControl label="Increment" clicked={() => this.props.onIncrementCounter()} />
                <CounterControl label="Decrement" clicked={() => this.props.onDecrementCounter()}  />
                <CounterControl label="Add 5" clicked={() => this.props.onAddCounter( 5 )}  />
                <CounterControl label="Subtract 5" clicked={() => this.props.onSubCounter( 5 )}  />
                <hr />
                <button onClick={()=> this.props.onSaveResult()} >Save result</button>
                <ul>
                    {this.props.results.map(result=> (
                        <li 
                            key={result.id}
                            onClick={()=> this.props.onDeleteSavedResult(result.id)}>
                                {result.value}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ctr : state.counter,
        results : state.results,
        products : state.products
    }
}

const mapsDispatchToProps = dispatch => {
    return {
        onIncrementCounter : ()=> dispatch({type : ActionType.INC_COUNTER}),
        onDecrementCounter : ()=> dispatch({type : ActionType.DEC_COUNTER}),
        onAddCounter : (value)=> dispatch({type : ActionType.ADD_COUNTER, val : value}),
        onSubCounter : (value)=> dispatch({type : ActionType.SUB_COUNTER, val: value}),
        onSaveResult : ()=> dispatch({type : ActionType.SAVE_COUNTER}),
        onDeleteSavedResult : (value)=> dispatch({type : ActionType.DELETESAVED_COUNTER, selectedLine : value}),
    }
}


export default connect(mapStateToProps, mapsDispatchToProps)(Counter);