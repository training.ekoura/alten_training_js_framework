const redux = require('redux')

const initialState = {
    counter : 0
}

// Reducer
const rootReducer = (currentState = initialState, action)=> {
    //TODO 

    if(action.type === 'INC_COUNTER'){
        return {
            counter : ++currentState.counter
        }
    }


    if(action.type === 'ADD_COUNTER'){
        return {
            counter : currentState.counter + action.value
        }
    }

    return currentState
}

// Store
const store = redux.createStore(rootReducer);
console.log(store.getState());

// subscription
store.subscribe(()=>{
    console.log('[Subscription]', store.getState());
})


// Dispatch Action
store.dispatch({ type : 'INC_COUNTER' })
store.dispatch({ type : 'ADD_COUNTER', value : 100 })



