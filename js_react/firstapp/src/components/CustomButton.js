import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types'

//const CustomButton = ({buttonName = "Show description", children})=>{
class CustomButton extends Component {

    render() {
        
        const {buttonName, children, onClickHandler, buttonType} = this.props

        const buttonRender = (buttonType === 'delete') ? 
                            <button style={{marginTop : "10px"}} className="btn btn-danger" onClick={onClickHandler}>{ buttonName }</button>
                        :
                            <button style={{marginTop : "10px"}} className="btn btn-success" onClick={onClickHandler}>{ buttonName }</button>
    
    
        return (
            <Fragment>
                {buttonRender}
                <div>{children}</div>
            </Fragment>
        )
    }

}

CustomButton.propTypes = {
    buttonName : PropTypes.string.isRequired,
    children : PropTypes.string,
    onClickHandler : PropTypes.func.isRequired,
    buttonType : PropTypes.string.isRequired
}

export default CustomButton