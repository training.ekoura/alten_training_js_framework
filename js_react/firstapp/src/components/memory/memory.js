import React, { Component, Fragment } from 'react'
import shuffle from 'lodash.shuffle'
import Card from './Card';
import PropTypes from 'prop-types';
import '../../App.css';

const SIDE = 6
const SYMBOLS = '😀🎉💖🎩🐶🐱🦄🐬🌍🌛🌞💫🍎🍌🍓🍐🍟🍿'
const VISUAL_PAUSE_MSECS = 750

class Memory extends Component {
    
    startCompteur = null

    componentDidUpdate() {
        if((this.state.start === true && this.state.counter === 0) || 
            (this.state.start === true && this.startCompteur === null)
        ){
            this.startCompteur = setInterval(()=>{
                this.setState({...this.state, counter : ++this.state.counter })
            }, 1000);
        }
        else if(this.state.start === false) {
            clearInterval(this.startCompteur)
            this.startCompteur =  null
        }
    }

    formatSeconde = (compteur) => {
        return Math.floor(compteur/60) + ':' + (compteur-(Math.floor(compteur/60)*60)) + " s"
    }
  
    state = {
        cards : this.generateCards(),
        currentPair: [],
        matchedCardIndices: [],
        counter : 0,
        essaie  : 0,
        start : false
    }

    generateCards() {
        const result = []
        const size = SIDE * SIDE
        const candidates = shuffle(SYMBOLS)
        while (result.length < size) {
            const card = candidates.pop()
            result.push(card,card)
        }
        const data = shuffle(result)
        return data
    }

    handleStart = ()=>{
        this.setState({...this.state, start : !this.state.start})
    }

    handleCardClick = (index) => {

        const { currentPair} = this.state
    
        if(currentPair.length === 2)
            return

        if(currentPair.length === 0){
            this.setState({currentPair:[index], essaie : ++this.state.essaie})
            return
        }

        this.handleNewPairClosedBy(index)
    }

    handleNewPairClosedBy(index) {
        const { cards, currentPair, matchedCardIndices } = this.state

        const newPair = [currentPair[0], index]
        const matched = cards[newPair[0]] === cards[newPair[1]]
        this.setState({ currentPair: newPair })
        if (matched) {
            this.setState({ matchedCardIndices: [...matchedCardIndices, ...newPair] })
        }
        setTimeout(() => this.setState({ currentPair: [] }), VISUAL_PAUSE_MSECS)
    }
    

    getFeedbackForCard(index){
        const { currentPair, matchedCardIndices } = this.state
        const indexMatched = matchedCardIndices.includes(index)

        if(currentPair.length < 2 ){
        return indexMatched || index === currentPair[0] ? 'visible' : 'hidden'
        }

        if(currentPair.includes(index)){
        return indexMatched ? 'justMatched' : 'justMismatched'
        }

        return indexMatched ? 'visible' : 'hidden'

    }

    render() {
        const { cards, matchedCardIndices } = this.state 
        //const won = matchedCardIndices.length === cards.length
        const won = matchedCardIndices.length === 2
        return (
            <div className="container-fluid">
                <hr />
                    <div style={{ display : 'flex', justifyContent : 'start' }}>
                        <button 
                            className={!this.state.start ? 'btn btn-success' : 'btn btn-danger'}
                            onClick={this.handleStart}>
                            {!this.state.start ? 'Start' : 'Stop'}
                        </button>
                    </div>
                    <h1>Temps : {this.formatSeconde(this.state.counter)} </h1>
                    <h1>Essaie : {this.state.essaie} </h1>
                <hr />
                <div className="memory">
                    {cards.map((card, index) => (
                    <Card key={index} index={index} card={card} feedback={this.getFeedbackForCard(index)} onClick={this.handleCardClick} />
                    ))}
                </div>
            </div>
        
        )
    }
}

Memory.protTypes = {
  name : PropTypes.string.isRequired
}

export default Memory;
