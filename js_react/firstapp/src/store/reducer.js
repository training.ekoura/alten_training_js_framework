import  { JSON_PRODUCTS } from "../api/products"
import * as ActionType from './action';

const initialState = {
    products : JSON_PRODUCTS,
    isAuth : true
}

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case ActionType.ADD_PRODUCT:
            return {
                ...state,
                products : [...state.products, action.value]
            }
        
        case ActionType.DELETE_PRODUCT:
            return {
                ...state,
                products : state.products.filter(product => product.productId !== action.selectedProductId)
            }
        
        case ActionType.UPDATE_PRODUCT:
            //let selectedProduct = state.products.filter(product => product.productId === +action.value.productId)
            let newProducts = []
            state.products.forEach(product => {
                if(product.productId === action.value.productId){
                    product = action.value
                }
                newProducts.push(product)
            })
            return {
                ...state,
                products : newProducts
            }
        
        case ActionType.LOGIN:
            return {
                ...state,
                isAuth : action.value
            }
       
    }

    return state
}

export default reducer